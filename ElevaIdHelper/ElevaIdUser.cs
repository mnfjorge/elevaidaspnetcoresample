﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ElevaIdHelper
{
    public class ElevaIdUser
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Username { get; set; }
        public string Doc { get; set; }
        public string Token { get; set; }
    }
}
