﻿using System;
using System.Collections.Generic;
using System.Security.Principal;
using System.Text;

namespace ElevaIdHelper
{
    public class ElevaIdIdentity : IIdentity
    {
        public ElevaIdIdentity(ElevaIdUser user)
        {
            User = user;
        }

        public ElevaIdUser User { get; private set; }

        public string AuthenticationType => "Bearer";

        public bool IsAuthenticated => true;

        public string Name => User.Name;
    }
}
