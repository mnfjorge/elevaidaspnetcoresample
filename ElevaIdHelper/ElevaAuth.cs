﻿using System;
using System.Collections.Generic;
using System.Net;

namespace ElevaIdHelper
{
    public static class ElevaAuth
    {
        private static object lockKey = new object();

        public static Dictionary<string, ElevaIdUser> Users = new Dictionary<string, ElevaIdUser>();

        public static ElevaIdUser Authenticate(string accessToken)
        {
            if (IsAuthenticated(accessToken))
                return Users[accessToken];

            return null;
        }

        public static bool IsAuthenticated(string accessToken)
        {
            lock (lockKey)
            {
                if (Users.ContainsKey(accessToken))
                    return true;

                var user = ValidateAccessToken(accessToken);
                if (user != null)
                {
                    if (!Users.ContainsKey(accessToken))
                        Users.Add(accessToken, user);
                    else
                        Users[accessToken] = user;

                    return true;
                }
            }

            return false;
        }

        private static ElevaIdUser ValidateAccessToken(string accessToken)
        {
            try
            {
                var client = new WebClient();
                client.Headers.Add(HttpRequestHeader.Authorization, "Bearer " + accessToken);
                var raw = client.DownloadString("https://id.elevaeducacao.com.br/me");
                var json = (dynamic)Newtonsoft.Json.JsonConvert.DeserializeObject(raw);

                if (json != null && json.email != null)
                {
                    return new ElevaIdUser
                    {
                        Name = json.name.ToString(),
                        Email = json.email.ToString(),
                        Doc = json.doc.ToString(),
                        Token = json.token.ToString(),
                        Username = json.username.ToString()
                    };
                }

                return null;
            }
            catch (Exception)
            {
                // ignored
            }

            return null;
        }
    }
}
