﻿using System;
using System.Collections.Generic;
using System.Security.Principal;
using System.Text;

namespace ElevaIdHelper
{
    public class ElevaIdPrincipal : IPrincipal
    {
        public ElevaIdPrincipal(ElevaIdUser user)
        {
            Identity = new ElevaIdIdentity(user);
        }

        public IIdentity Identity { get; private set; }

        public bool IsInRole(string role) => true;
    }
}
