﻿using ElevaIdHelper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ElevaIdNetCoreIntegration.Filters
{
    public class ElevaIdAuthorizationAttribute : AuthorizeAttribute, IAuthorizationFilter
    {
        public void OnAuthorization(AuthorizationFilterContext context)
        {
            var accessToken = context.HttpContext.Request.Cookies["auth"];

            if (context.HttpContext.Request.Query.ContainsKey("access_token"))
                accessToken = context.HttpContext.Request.Query["access_token"].ToString();

            if (!string.IsNullOrWhiteSpace(accessToken))
            {
                var user = ElevaAuth.Authenticate(accessToken);
                if (user != null)
                {
                    context.HttpContext.Response.Cookies.Append("auth", accessToken);
                    context.HttpContext.User = new System.Security.Claims.ClaimsPrincipal(new ElevaIdPrincipal(user));
                    return;
                }
            }

            var currentUrl = $"{context.HttpContext.Request.Scheme}://{context.HttpContext.Request.Host}{context.HttpContext.Request.Path}{context.HttpContext.Request.QueryString}";

            context.Result = new RedirectResult("https://id.elevaeducacao.com.br/login?redirect_uri=" + currentUrl);
        }
    }
}
