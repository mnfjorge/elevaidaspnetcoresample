﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ElevaIdHelper;
using Microsoft.AspNetCore.Mvc;

namespace ElevaIdNetCoreIntegration.Controllers
{
    public class AuthController : Controller
    {
        public IActionResult Login(string access_token)
        {
            if (!string.IsNullOrWhiteSpace(access_token))
            {
                var user = ElevaAuth.Authenticate(access_token);

                //TODO: verificar aqui se o usuario tem acesso ao sistema, se quiser validar permissoes

                if (user != null)
                {
                    Response.Cookies.Append("auth", access_token);

                    return RedirectToAction("Index", "Home");
                }
            }

            return Redirect("https://id.elevaeducacao.com.br/login?redirect_uri=" + Url.Action("Login", "Auth", null, Url.ActionContext.HttpContext.Request.Scheme));
        }
    }
}